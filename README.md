# Título del Proyecto
### Participante:
<p>Nombre:</p>
<p>Correo:</p>
<p>Departamento: Unidad de Comunicaciones.</p>

## Introducción
<p>Escribir de manera breve la intención del proyecto.</p>

## Objetivos
<p>Lo que se espera alcanzar con el proyecto.</p>

## Descripción (funcionamiento)
<p>Redactar como funciona a grosso modo, como funciona, no es importante ser extremadamente detallado, para eso se utilizará el doc que anexen al proyecto.</p>

## Estado Actual
<p>Escribir que actividades ya se realizaron en el proyecto y concluir la sección con las actividad en la que se encuentran trabajando justo en el momento de la redacción de este archivo.</p>

## Modo de implementación
<p>De manera clara, detallada y estructurada, redactar como poner en funcionamiento este proyecto, sin importar como se realizo el proyecto, lo importante es como ponerlo a ejecutar, donde colocar los archivos y/o carpetas y con que comandos se logra poner en funcionamiento el proyecto.</p>
<br/>
## [Comentarios extras]
<p>Algún tipo de comentario extra o sección que no se encuentre en esta plantilla, pero sea necesario anexarla.</p>
<code> Por si necesitan subir códigos o comandos utilicen estas etiquetas</code>